/// <reference types="cypress" />

const numberUrl = 10000

it('should check url only with status code 200',() =>{
    const report = []

    for(let i = 1250; i < numberUrl; i++) {

        cy.request ({
            url:`https://inardex.sk/serial/c${i}/`,
            failOnStatusCode: false
        })

        .should((response) => {
            if(response.status === 200) {
                report.push({url:`https://inardex.sk/serial/c${i}/`, statusCode:response.status}) 
                cy.writeFile('cypress/fixtures/adress2.json', report)
            }
        })
    }
}) 