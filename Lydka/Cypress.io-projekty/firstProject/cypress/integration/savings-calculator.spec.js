/// <reference types="cypress" />

it('should by calculate total income', () => {
    //otvorit stranku
    cy.visit('/savingscalculator.php')
    //zvolit fond
    cy.get('#fundSelect').select('Tom & Jerry corp')
    //zadat kolko chcem vlozit
    cy.get('#oneTimeInvestmentInput').type(10000)
    //zadat pocet rokov
    cy.get('#yearsInput').type(10)
    //klikneme na tlacidlo calculate
    //cy.get('[data-test=calculate]').click()
    cy.contains('Calculate').click()
    //overime ze total income nie je prazdne a ze obsahuje "kr" ako menu
    //cy.get(':nth-child(1) > p').should('not.be.empty')
    //.should('contain.text', 'kr')
    cy.get('div.result')
        .find('div')
        .first()
        .find('p')
        .should('contain.text', 'kr')
        .should('not.be.empty')
    //overime interest income nie je prazdny a obsahuje kr ako menu
    cy.get('div.result')
        .find('div')
        .eq('1')   //vyhlada podla poradia elementu
        .find('p')
        .should('not.be.empty')
        .should('contain.text', 'kr')
        
    })


