/// <reference types="cypress" />


let token = "Bearer BQCvtQDbvtxhdXr7JFwV6cLS5YX3QQwaA00BXLO6oBdr-c2WKJ4igPghzPmA4-E_Un3nc6XiAppQZSaNutDqrtX0N8qnHqkrB8P5boHjNYHppV_3nCdcO5RO6MmpGFN-PmjiFpD8yyUxmVSsW-PPcTlllFqSH7VrZ9s"

//test collection

describe("Basic Spotify Test", () =>{

    //first test

    

    it("should find a playlist name & ID", () =>{
        cy.request({
            method: "GET",
            url: "https://api.spotify.com/v1/me/playlists",
            headers: {
                "Authorization": token,
                "Content-Type": "application/json"
            }
        })
        .should((response) => {
            expect(response.status).to.eq(200)
            cy.log(response.body)
            expect(response.body.items[0].owner.display_name).to.eq("MiLyd")
            expect(response.body.total).not.to.eq(0)
        })
    })
})