let criminals = [{
    firstName: 'Michal',
    secondName: 'Zelený',
    birth: 1987,
    address: 'Dlhá 56',
    city: 'Prievidza'
},{
    firstName: 'Monika',
    secondName: 'Ružová',
    birth: 1984,
    address: 'Muškátová 18',
    city: 'Bratislava'
},{
    firstName: 'Simona',
    secondName: 'Čierna',
    birth: 1976,
    address: 'Slovenská 6',
    city: 'Košice'
}]


let menoPodozrivého = prompt('Zadaj meno podozrivého')

let udajePodozriveho = criminals.findIndex(function(podozrivy){
    return podozrivy.firstName === menoPodozrivého
})
let nasPodozrivy = criminals[udajePodozriveho]

console.log(`Meno: ${nasPodozrivy.firstName}`)
console.log(`Priezvisko: ${nasPodozrivy.secondName}`)
console.log(`Narodený: ${nasPodozrivy.birth}`)
console.log(`Adresa: ${nasPodozrivy.address}`)
console.log(`Mesto: ${nasPodozrivy.city}`)