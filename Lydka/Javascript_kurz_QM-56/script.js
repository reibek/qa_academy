/*
let firstBook = {
    title: "Harry Potter a kamen mudrcov",
    author: "J. K. Rowling",
    published: 1997
}

let secondBook = {
    title: "Harry Potter a tajomná komnata",
    author: "J. K. Rowling",
    published: 1998 
}

let resultBook = function(book){
    console.log(`Kniha od ${book.author} z roku ${book.published}`)
}

resultBook(firstBook)
resultBook(secondBook)
*/

let person1 = {
    name: "Lýdia Lukas",
    age: 36,
    city: "Bratislava",
    gender: "female"
}

let person2 = {
    name: " Michal Banaš",
    age: 30,
    city: "Bratislava",
    gender: "male"
}

let person3 = {
    name: "Žofia Gulagič",
    age: 12,
    city: "Košice",
    gender: "female"
}

let oslovenie = function(data){
    if (data.gender === "female") {
        console.log(`Volá sa ${data.name}. Je jej ${data.age} rokov a pochádza z mesta ${data.city}`);
        
    } else {
        console.log(`Volá sa ${data.name}. Je mu ${data.age} rokov a pochádza z mesta ${data.city}`);
    }

}

oslovenie(person3)