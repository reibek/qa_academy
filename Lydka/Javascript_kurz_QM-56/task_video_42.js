let vládneNariadenie = 'neplatné'

let school = {
    type: 'Združená stredná škola',
    street: 'Račianska 105',
    city: 'Bratislava',
    capacity: 300,
    open: function(){
        return `Škola- ${this.type}, ${this.street}, ${this.city} je otvorená`
    },
    closed: function(stav){
        return `Škola- ${this.type}, ${this.street}, ${this.city} je zatvorená`
    }
}

if (vládneNariadenie === 'platné') {
    let resultClosed=school.closed ()
    console.log(resultClosed);
} else {
    let resultOpen= school.open ()
    console.log(resultOpen)
}
