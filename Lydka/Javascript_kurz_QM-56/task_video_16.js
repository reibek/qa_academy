//podmienka IF a ELSE, ELSE IF

let job = 'programátor'

if(job === 'programátor'){
    console.log('Aký je rozdiel medzi HTML a CSS?')
} else if(job === 'administrátor') {
    console.log('Aké druhy sietí poznáte?')
} else if(job === 'sekretárka') {
    console.log('Aké poznáte funkcie v exceli?')
} else {
    console.log('Na túto pozíciu nie sú pripravené otázky.')
}
