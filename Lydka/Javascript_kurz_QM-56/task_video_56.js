/*let toDo = ['vyvenčiť psa', 'nakúpiť', 'naučiť sa JavaScript', 'urobiť si obed' ]

    toDo.forEach(function(task, index){
    console.log((index+1)+task)
 })
*/


//ALEBO:

let toDo = ['vyvenčiť psa', 'nakúpiť', 'naučiť sa JavaScript', 'urobiť si obed' ]

toDo.forEach(function(task,index){
    let number = index + 1
    console.log(`${number}. ${task}`)
})

