let person1 = {
    Name: 'Lýdia',
    Age: 36,
    City: 'Bratislava',
    Gender: 'Female'
}

let person2 = {
    Name: 'Michal',
    Age: 30,
    City: 'Prievidza',
    Gender: 'Male'
}

let person3 = {
    Name: 'Žofka',
    Age: 12,
    City:'Košice',
    Gender: 'Female'
}


let predstavenie = function (osoba) {
    if(osoba.Gender === 'Female') {
    console.log(`Volá sa ${osoba.Name}. Je jej ${osoba.Age} a pochádza z mesta ${osoba.City}.`)
} else{
    console.log(`Volá sa ${osoba.Name}. Je mu ${osoba.Age} a pochádza z mesta ${osoba.City}.`);
}
}


predstavenie(person1)   