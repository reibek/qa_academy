let criminals = [{
    firstName: 'Michal',
    secondName: 'Zelený',
    birth: 1987,
    licencePlate: 'PD565MN',
    address: 'Dlhá 56',
    city: 'Prievidza'
},{
    firstName: 'Monika',
    secondName: 'Ružová',
    birth: 1984,
    licencePlate: 'BA322TU',
    address: 'Muškátová 18',
    city: 'Bratislava'
},{
    firstName: 'Simona',
    secondName: 'Čierna',
    birth: 1976,
    licencePlate: 'TB822PO',
    address: 'Slovenská 6',
    city: 'Košice'
}]


let find = prompt('Zadaj údaje')

let result = criminals.filter(function(oneSuspect){
    let findSuspect = oneSuspect.licencePlate.toLowerCase().includes(find)
    return findSuspect
})

result.forEach(function(allResult){
    console.log(`Meno: ${allResult.firstName}`)
    console.log(`Priezvisko: ${allResult.secondName}`)
    console.log(`Dátum narodenia: ${allResult.birth}`)
    console.log(`Poznávacia značka auta: ${allResult.licencePlate}`)
    console.log(`Ulica: ${allResult.address}`)
    console.log(`Mesto: ${allResult.city}`)
    console.log(`----------------------------------`);
})
