let school = {
    type: 'Združená stredná škola',
    street: 'Račianska 105',
    city: 'Bratislava',
    capacity: 300,
    open: function(){
        console.log('Škola je otvorená')
    },
    closed: function(stav){
        return `Škola je ${stav}`
    }
}

console.log(school.type)
console.log(school.city)

school.open()

let result=school.closed('zatvorená')
console.log(result);