
//vypisanie do konzole
console.log("ahoj ja som sima")
console.log("milujem cokoladu")


// string a premenne
let firstname = "Sima"
let secondname = "Drinkova"
let city = "Bratislava"
console.log(firstname + " " + secondname + "," + city)


//zadanie
let firstname = "Sima"
let job = "HR Specialist"
console.log("Ahoj, volam sa " + firstname + " a pracujem ako " + job)


//number
let age = 26
console.log(age)


//Boolean (true or false)
let firstname = "sima"
let age = 21
let adult = age >= 18
console.log(adult)


/*=== rovna sa 
!== nerovna sa
< mensie nez (<=)
> vacsie nez (>=)
*/

// zadanie
let age = 33
let child = age <= 18
let adult = age >= 18
let senior = age >= 65
console.log("child:" + child)
console.log("adult:" + adult)
console.log("senior:" + senior)


//podmienka if 
let age = 20
if(5 > 0) {
    console.log("je to pravda")
}
if (age >= 18) {
    console.log("si dospely")
}
if (age <= 18) {
    console.log("si dieta")
}



// if a else - ak je true bude if, ak je false tak je else
let age = 20

if (age >= 18) {
    console.log("si dospely")
}  else {

    console.log("si dieta")
}


let firstname ="david"
if(firstname === "david"){
    console.log("ahoj david")
} else {
    console.log("nepoznam ta")
}



// if a else, else if
let firstname = "Peter"

// david, martin, jana

if(firstname === "david"){
    console.log("dobry den davide, mozes ist dalej")
} else if(firstname === "Martin"){
    console.log("Dobry den martine, mozes ist dalej")
} else if(firstname === "Jana"){
    console.log("dobry den jano, mozes ist dalej")

}else{
    console.log("nemozete vstupit. Nemate opravnenie")
}


// zadanie
let job = "sekretarka"

if(job === "programator"){
    console.log("Aky je rozdiel medzi html,css a java?")
}else if(job=== "admin"){
    console.log("ake druhy siete poznas?")
}else if(job==="sekretarka"){
    console.log("excel?")
}


// a zaroven &&  
let firstname = "David"
let height = 185

if(firstname === "david" && height === 185){
    console.log("je podozrely")
}


true = true, true
false = false, true
false = true, false
false = false, false 

// alebo || 

if(firstname === "david" || height === 185){
    console.log("je podozrely")
}

true = true, true
true = false, true
true = true, false
false = false, false 


//
//scope 
// globalny (pouzijes vsade)a lokalny(len v bloku)
let prvni = "prvy text"
// if vytvori blok
if(true){
    console.log(prvni)
    let druha = "druhy text"
}

console.log(druha)



// Funkcie
function    pozdrav(){          
    console.log("pozdrav 1 ")
} 

pozdrav()

let pozdrav2= function(){           // alebo
    console.log("toto je druhy pozdrav")
}
pozdrav2()
                                    // alebo
let pozdrav3=   () =>console.log("toto je pozdrav3")
pozdrav3()



// funkcie - parameter a argument

// funkcie - return
let naDruhu = function(number){
    let result = number * number
    return result   
    
}

let value1 = naDruhu(5)
value1= value1  +   50
console.log(value1)


//
let adultChecker = function(age){
    if(age >= 18){
        let result  ="dospely"
        return  result
    }else {
        let result = "dite"
        return  result
    }
}
let value = adultChecker(10)

if(value === "dospely"){
    console.log("vysledok kontroly veku:" + value)
    console.log("mozes vstupit")
}else{
    console.log("vysledok kontroly veku:" + value)
    console.log("nemozes vstupit")
}


// undefined a null - hovoria ze chyba hodnota 
// undefined v premennej

let age 
if(age === undefined){
    console.log("prosim vyplnte vek")
}else {
    console.log(age)
}

// undefined ako argument funkcie
let mojeFunkce = function(num){
    console.log(num)
}
mojeFunkce()

// undefined u return
let mojeFunkce2 =   function(num){
    return  num
}

let value = mojeFunkce2()
console.log(value)

// null ako prirodzena hodnota
let myAge = 30
myAge = null
console.log(myAge)


// Globalny Scope

// Globální scope (num1, num2, num3)
// Lokální scope (a, b, c)
// Lokální scope (myName)
let num1 = 5
let num2 = 3
let num3 = 8
let pristup = function(a, b, c){
    if(a === num1 && b === num2 && c === num3){
        console.log("Můžete vstoupit")
    } else {
        console.log("Špatně zadaný kód. Zkuste to prosím znovu")
    }
}
pristup(5,3,8)


//Template strings

let myName= "harry potter"
let age = 15
console.log("ahoj, ja som " + myName + " a je mi  " + age + " rokov ")
console.log(`ahoj, ja som ${myName} a je mi ${age} rokov. `)

let pozdrav = function(firstname, lastname){
    console.log(`ahoj, moje meno je ${firstname} a moje priezvisko je ${lastname}`)
}

pozdrav("david", "setek")


//
let movie = "ospala diera"
let director = "tim burton"
let award = "najlepsi vykon vo vyprave"

console.log(`videl som film ${movie}, ktory reziroval ${director}. Tento film ziskal ocenenie za ${award}`)



//
// objekty
let myBook = {
    title: "Harry Potter",
    author: "J.K. Rowling",
    published: 1997
}

console.log(myBook.title)
console.log(myBook.author)
console.log(myBook.published)

console.log(`${myBook.title} je kniha od autorky ${myBook.author} a bola vydana v roku ${myBook.published}`)


//
//objekty pomocou funkcie
let firstBook = {
    title: "Harry Potter",
    author: "J.K. Rowling",
    published: 1997
}

let secondBook = {
    title: "Harry Poter a Tajemna komnata",
    author: " J.K.Rowling",
    published: 1998
}

let resultBook =  function(book){
    console.log(`Kniha od ${book.author} z roku ${book.published} sa vola ${book.title}`)
}
resultBook(firstBook)
resultBook(secondBook)


//
// proměnné a objekty
let firstName = "David"
let myObject = {
    firstName: firstName,
    secondName: "Šetek",
    age: 34
}

console.log(myObject.firstName)


// Objekty a return
let firstBook = {
    title: "Harry Potter a kámen mudrců",
    author: "J. K. Rowlingová",
    published: 1997
}

let secondBook = {
    title: "Harry Potter a Tajemná komnata",
    author: "J. K. Rowlingová",
    published: 1998
}

let bookInfo = function(book){
    return {
basicInfo: `${book.title} od ${book.author}`,
publishing: `Kniha ${book.title} byla vydána v roce ${book.published}`
    }
}

let result = bookInfo(firstBook)
console.log(result.basicInfo)
console.log(result.publishing)


// Metody - funkcia, ktora je priradena k objektu
let person1 = {
    firstName: "David",
    secondName: "Šetek",
    age: 34,
    height: 184,
    salary: 10000,
    greet: function(friends){ //metoda //parameter
        console.log("Ahoj, mám " + friends + " přátel")
    },
    toWork: function(job){
        return "Jdu do své práce, což je " + job
    }
    }
    console.log(person1.firstName)
    console.log(person1.height)
    person1.greet(10)   //argument
    let result = person1.toWork("programátor") //pri return musi byt result
    console.log(result)
  


 // let zadanie, vytvori premennu
    let school= {
        type: "gymnazium",
        street: "pankuchova",
        city: "bratislava",
        capacity: 16,
        open: function(){
            console.log("Skola je otvorena")
        },
        closed: function (){
            return "Skola je zatvorena"
        }
    }
    console.log(school.type)
    console.log(school.city)
    school.open()
    let result = school.closed()
    console.log(result)
    

    //this
    let person1 = {
        firstName: "David",
        secondName: "Šetek",
        age: 34,
        greet:function(){
            console.log(this.firstName)
        }
    }

    person1.greet()
    

    let person2 = {
        firstName: "Harry",
        secondName: "Potter",
        age: 15,
        greet:function(){
            console.log(this.firstName)
        }
    }
    person2.greet()
       


