*** Settings ***
Library     SeleniumLibrary
Library     RequestsLibrary

*** Variables ***
${id}       2

*** Test Cases ***

Quick Get User
        create session  user    https://reqres.in/
        ${params}=     Create Dictionary        id=${id}
        ${resp}=        get on session    user    /api/users/   params=${params}

        log to console      ${resp.content}
        log to console      ${resp.status_code}


