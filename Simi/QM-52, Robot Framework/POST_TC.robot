*** Settings ***
Library     RequestsLibrary
Library     Collections


*** Variables ***
${base_url}         https://reqres.in

*** Test Cases ***

Request Create User

    create session  user    ${base_url}
    ${body}=        create dictionary       name=morpheus       job=leader
    ${header}=      create dictionary       Content-Type=application/json
    ${response}=    post request    user     /api/user       data=${body}    headers=${header}

    log to console  ${response.status_code}
    log to console  ${response.content}

    #VALIDATIONS
    ${status_code}  convert to string   ${response.status_code}
    Should be equal     ${status_code}      201
    Should not be empty     ${body}





