/// <reference types="cypress" />



    it("oznac elementy", () =>{

        cy
        .visit("/")

        cy
        .get(".background_title")

        cy
        .contains("My Boards")

        cy
        .get(".board_title")

        cy
        .contains("novy board")
    })


///klikanie,pisanie,interakcia
  it("vytvorenie noveho zoznamu", () => {

    cy
    .visit("/board/34286315570")

    cy
    .get(".CreateList_title")
    .type("nakupik{enter}")

})


it("vytvorenie tasku", ()=>{

    cy
    .visit("/board/34286315570")

    cy
    .contains("Add a card...")
    .type("mlieko{enter}")
})


it.only(" oznacenie boardu", ()=>{

    cy
    .visit("/board/34286315570")

    cy
    .get(".Task Input")
    .uncheck()
})




///jednoduche overovanie

beforeEach( () => {

    cy
      .visit("/board/34286315570")
  
  })

  it("vytvorenie listu", ()=>{

    cy
    .contains("Add a list...")
    .type("dovolenka{enter}")

  })

  it("overenie poctu listov", ()=>{

    cy
    .get(".taskTitle")
    .should("have.length", 2)
  })

it("odstranenie listu", ()=>{

    cy
    .get('[data-id="80153597189"] .dropdown')
    .click()

    cy
    .get('[data-id="80153597189"] .delete')
    .click()
    
})

it("overenie odskrtnutia", ()=>{

    cy
  .get('.Task .checkmark')
  .uncheck()

  cy
  .get('.Task .checkmark')
  .should("not.be.checked")
})

it.only("overenie textu na pridanie tasku", ()=>{

    cy
    .get(".List_addTask")
    .should("contain.text", "Add a card")
  })


///retry vztahy medzi prikazmi

it("pridanie polozky do druheho zoznamu", ()=>{

    cy
    .visit("/board/34286315570")
    .contains("Add a card...")

    cy
    .get('[data-id="18444370055"]')
    .contains("Add a card...")
    .type("maslo{enter}")
    
})


/// pokrocile overovanie

it("over vsetky zaskrtnute tasky", ()=>{

    cy
    .visit("/board/34286315570")
    cy
    .get(".Task input").then(tasks =>{

        expect(tasks[0]).to.be.checked
    })
})


/// sledovanie http requestov

 beforeEach( () =>{

    cy
    .server()

    cy
    .route({
        method: "POST",
        url: "/api/tasks",
    }).as("createTask")

    cy
    .route({
        method: "DELETE",
        url: "/api/lists/*"
    }).as("deleteLists")

    cy  

  .route("PATCH", "/api/tasks/*")
  .as("editTask")

    cy
    .visit("/board/34286315570")
})

it("vytvorenie tasku", ()=>{

    cy
    .contains("Add a card...")
    .type("papuce{enter}")

    cy
    .wait("@createTask")
    .its("status")
    .should("eq", 201)
        
    })

   it("odstranenie zoznamu", ()=>{



    cy
    .wait("@deleteLists")


   }) 


   it.only('zaskrtnutie tasku', () => {

    cy
    .get(".Task input")
    .check()
  
    cy
    .wait("@editTask")
    .then( task => {
  
      expect(task.status).to.eq(200)
      expect(task.request.body.completed).to.be.true
      
    })
})




/// nahradzovanie requestov
beforeEach ( () => {

    cy  
    .server()

    cy
    .route("/api/boards", "fx:twoboards" )
    .as("boardList")

    cy
    .visit("/")
})

it("zoznam dvoch boardov z fixture")



/// odoslanie http request


it("vytvorenie boardu cez api", ()=>{

    cy
    .request({
        method:"POST",
        url: "/api/boards",
        body:{
            name: "board cez api"
        }
    })


    cy
    .visit("/")
})
