/// <reference types="Cypress"/>



it("get users", ()=>{

    cy
    .request("https://reqres.in/api/users?page=2").then((res)=>{

    expect(res.status).equal(200)
    expect(res.body.data).not.empty
    expect(res.body.data[0]).has.property("id", 7)
    expect(res.body.data[0]).has.property("first_name", "Michael")
    console.log(res.body.data[0])

    })

    


})

it("get single user", ()=>{

    cy
    .request("https://reqres.in/api/users/2").then((res)=>{
        expect(res.status).eq(200)
        expect(res.body.data).has.property("first_name", "Janet")
        console.log(res.body)
        expect(res.body.data).has.property("last_name", "Weaver")
    })
})

it.only("list resources", ()=>{

    cy
    .request("https://reqres.in/api/unknown").then((res)=>{
        expect(res.status).eq(200)
        expect(res.body).not.be.empty
        console.log(res.body)
        expect(res.body.total).eq(12)
        expect(res.body.data[0]).has.property("name", "cerulean")
        
    })
})

