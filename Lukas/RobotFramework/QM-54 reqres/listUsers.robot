*** Settings ***
Library  RequestsLibrary
Test Setup  Log To Console    Starting testCase
Test Teardown   Log To Console    ${status_code}
*** Variables ***
${url}  https://reqres.in



*** Test Cases ***
List Users API
    API session
    ${param}=    Create Dictionary   page=2
    ${resp}=  GET On Session  Users   /api/users  ${param}
    ${data}=    Set Variable    ${resp.json()}[data]
    Log To Console    ${data}

    #VALIDATIONS
    ${status_code}=  Convert To String    ${resp.status_code}
    Should Be Equal    ${status_code}   200
    ${total_pages}=  Convert To String  ${resp.json()}[total_pages]
    Should Be Equal    ${total_pages}    2
    ${total}=   Convert To String    ${resp.json()}[total]
    Should Be Equal    ${total}    12
    ${page}=    Convert To String    ${resp.json()}[page]
    Should Be Equal    ${page}    2
    ${per_page}=    Convert To String    ${resp.json()}[per_page]
    Should Be Equal    ${per_page}    6


*** Keywords ***
API session
    Create Session    Users    ${url}