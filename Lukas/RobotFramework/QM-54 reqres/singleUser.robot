*** Settings ***
Library  RequestsLibrary
Test Setup  log to console  Starting testCase
Test Teardown  log to console  Closing testCase, status ${resp.status_code}

*** Variables ***
${url}  https://reqres.in
${userid}   2


*** Test Cases ***
GET Single user from DB
    API Session
    ${resp}=    GET on Session  single user  /api/users/${userid}
    Log To Console    ${resp.content}

    #VALIDATIONS
    ${respstatus}=  Convert To String    ${resp.status_code}
    ${respid}=  Convert To String    ${resp.json()}[data][id]
    ${respemail}=   Convert To String    ${resp.json()}[data][email]
    ${respfirstname}=   Convert To String    ${resp.json()}[data][first_name]
    ${resplastname}=    Convert To String    ${resp.json()}[data][last_name]
    ${respavatar}=  Convert To String    ${resp.json()}[data][avatar]
    ${resptext}=    Convert To String    ${resp.json()}[support]

    Should Be Equal    ${respstatus}    200
    Should Be Equal    ${respid}    2
    Should Be Equal    ${respemail}    janet.weaver@reqres.in
    Should Be Equal    ${respfirstname}    Janet
    Should Be Equal    ${respavatar}    https://reqres.in/img/faces/2-image.jpg
    Should Contain    ${resptext}    ReqRes

GET Single user - not found

    API Session
    ${resp}=    GET On Session  single user  /api/users/23

*** Keywords ***
API Session
    Create Session    single user    ${url}