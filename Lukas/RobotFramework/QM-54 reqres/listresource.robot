*** Settings ***
Library  RequestsLibrary
Test Setup  log to console  Starting testCase
Test Teardown  log to console  Closing testCase, status ${resp.status_code}

*** Variables ***
${url}  https://reqres.in
${relativeurl}  /api/unknown

*** Test Cases ***
GET resources list
    API Session
    ${resp}=  GET On Session  list   ${relativeurl}
    Log To Console    ${resp.content}
    
    #VALIDATIONS
    ${respid1}=  Convert To String    ${resp.json()}[data][0][id]
    ${respname1}=   Convert To String    ${resp.json()}[data][0][name]
    Should Be Equal    ${respid1}    1
    Should Contain    ${resp.json()}[data][0][name]    cerulean
    ${respcolor5}=  Convert To String    ${resp.json()}[data][4][color]
    Should Contain    ${respcolor5}    E2583E


*** Keywords ***
API Session
    Create Session    list    ${url}
