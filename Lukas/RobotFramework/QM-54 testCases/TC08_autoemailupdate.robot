*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  OperatingSystem
Library  DateTime
Suite Setup  Log To Console    Opening browser
Suite Teardown  Log To Console    Closing browser
Test Setup      log to console  Starting test
Test Teardown   log to console  Closing test

*** Variables ***
${base_url}  https://simple-books-api.glitch.me

*** Test Cases ***
API customer registration
    ${date}=    Get Current Date    UTC
    ${convertDate}=  Convert Date    ${date}    result_format=%H%M%S
    ${convert}=  Convert To String    ${convertDate}
    ${json}=    Get File    C:/Users/aiwa/PycharmProjects/Automatization/TestCases/signup.json
    ${object}=  Evaluate  json.loads('''${json}''')  json
    ${stringClientEmail}=  Set Variable  ${convert}${object}[clientEmail]

    API registrating client Session
    ${body}=    Create Dictionary   clientName=${object}[clientName]  clientEmail=${stringClientEmail}
    ${header}=  Create Dictionary   Content-Type=application/json
    ${resp}=    Post Request    register API client    /api-clients     data=&{body}   headers=${header}

    Log To Console    ${resp.status_code}
    Log To Console    ${resp.content}

    #VALIDATIONS
    ${resp_status_code}=    Convert To String    ${resp.status_code}
    Should Be Equal    ${resp_status_code}    201
    ${resp_body}=   Convert To String    ${resp.content}
    Should Contain    ${resp_body}    accessToken




*** Keywords ***
API registrating client Session
    Create Session    register API client    ${base_url}