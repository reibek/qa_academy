*** Settings ***
Library  RequestsLibrary

*** Variables ***
${token}    5c8b767325b7f50761c0a4c4da79786d
${mesto}    Zilina

*** Keywords ***

Weatherapp session
    Create Session  weatherapp     http://api.openweathermap.org

*** Test Case ***

Quick Get Request Weatherapp service
    Weatherapp session
    &{params}=   Create Dictionary   q=${mesto}     appid=${token}
    ${resp}=     GET On Session  weatherapp  /data/2.5/weather    params=${params}
    ${cityName}=  Set Variable  ${resp.json()}[name]
    ${weather}=  Set Variable   ${resp.json()}[weather]
    Set Global Variable    ${idCity}  ${resp.json()}[id]

    #VALIDATIONS
    ${status_code}=  convert to string  ${resp.status_code}
    Should Be Equal    ${status_code}   200
    Should Be Equal    ${cityName}   ${mesto}
    Should Not Be Empty    ${weather}

5 day forecast by city ID saved to variable in previous test
    Weatherapp session
    &{params}=  Create Dictionary   id=${idCity}     appid=${token}
    ${resp}=    GET On Session  weatherapp    /data/2.5/forecast  params=&{params}
    ${cityName}=  Set Variable  ${resp.json()}[city][name]
    ${cnt}=   Set Variable  ${resp.json()}[cnt]
    Log To Console    ${resp.content}

    #VALIDATIONS
    ${status_code}=  convert to string  ${resp.status_code}
    Should Be Equal    ${status_code}   200
    Should Be Equal    ${cityName}   ${mesto}
