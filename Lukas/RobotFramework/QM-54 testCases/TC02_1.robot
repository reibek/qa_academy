*** Settings ***
Library  RequestsLibrary

*** Variables ***
${token}    5c8b767325b7f50761c0a4c4da79786d
${city}    Zilina

*** Keywords ***

Weatherapp session
    Create Session  weatherapp     http://api.openweathermap.org

*** Test Case ***

Get current weather for city with name specified in variables 
    Weatherapp session
    &{params}=   Create Dictionary   q=${city}     appid=${token}
    ${resp}=     GET On Session  weatherapp  /data/2.5/weather    params=${params}
    ${cityName}=  Set Variable  ${resp.json()}[name]
    ${weather}=  Set Variable   ${resp.json()}[weather]
    Set Global Variable    ${idCity}  ${resp.json()}[id]

    #VALIDATIONS
    ${status_code}=  convert to string  ${resp.status_code}
    Should Be Equal    ${status_code}   200
    Should Be Equal    ${cityName}   ${city}
    Should Not Be Empty    ${weather}

Get 5 day forecast by city ID for city specified in previous test
    Weatherapp session
    &{params}=  Create Dictionary   id=${idCity}     appid=${token}
    ${resp}=    GET On Session  weatherapp    /data/2.5/forecast  params=&{params}
    ${cityName}=  Set Variable  ${resp.json()}[city][name]
    ${cnt}=  Convert To String    ${resp.json()}[cnt]

    #VALIDATIONS
    ${status_code}=  convert to string  ${resp.status_code}
    Should Be Equal    ${status_code}   200
    Should Be Equal    ${cityName}   ${city}
    Should Be Equal    ${cnt}   40
