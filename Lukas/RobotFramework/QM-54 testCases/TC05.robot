*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${base_url}  https://reqres.in

*** Test Cases ***
Reqres.in Post API
    Create Session    reqres    ${base_url}
    ${body}=    Create Dictionary   name=morpheus   job=leader
    ${header}=  Create Dictionary   Content-type=application/json
    ${response}=    Post Request   reqres    /api/users   data=${body}  headers=${header}

    Log To Console    ${response.status_code}
    Log To Console    ${response.content}

*** Keywords ***
