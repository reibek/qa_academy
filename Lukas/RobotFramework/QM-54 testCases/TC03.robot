*** Settings ***
Library  RequestsLibrary


*** Variables ***
${token}    5c8b767325b7f50761c0a4c4da79786d
${mesto}    Zilina
${lon}  19.1667
${lat}  49.1667

*** Test Cases ***
Air pollution API test
    API Session
    &{params}=  Create Dictionary   lat=${lat}  lon=${lon}  appid=${token}
    ${resp}=    GET On Session  air pollution    /data/2.5/air_pollution  params=&{params}
    ${list}=  Set Variable    ${resp.json()}[list]
    Log To Console    ${list}[0][dt]


    #VALIDATIONS
    ${statusCode}=  Convert To String    ${resp.status_code}
    Should Be Equal    ${statusCode}    200
    ${coord}=  Convert To String    ${resp.json()}[coord][lon]
    Should Be Equal    ${coord}  19.1667
    ${sakra}=   Convert To String    ${list}
    Should Be Equal    ${list}[0][dt]    1638273600



*** Keywords ***
API Session
    Create Session    air pollution    https://api.openweathermap.org
