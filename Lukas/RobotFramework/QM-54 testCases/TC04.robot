*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  OperatingSystem

*** Variables ***
${base_url}  https://simple-books-api.glitch.me

*** Test Cases ***
API customer registration
    API registrating client Session
    &{body}=  Create Dictionary     clientName=Valerian  clientEmail=valerianjlzyhkj@exahmple.com
    ${header}=  Create Dictionary   Content-Type=application/json
    ${resp}=    Post Request    register API client    /api-clients     data=&{body}    headers=${header}
    
    Log To Console    ${resp.status_code}
    Log To Console    ${resp.content}
    
    #VALIDATIONS
    ${resp_status_code}=    Convert To String    ${resp.status_code}
    Should Be Equal    ${resp_status_code}    201
    ${resp_body}=   Convert To String    ${resp.content}
    Should Contain    ${resp_body}    accessToken

*** Keywords ***
API registrating client Session
    Create Session    register API client    ${base_url}
    