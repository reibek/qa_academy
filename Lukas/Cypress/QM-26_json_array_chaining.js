/// <reference types="Cypress" />

describe('Pass JSON/array from one request to another', () => {
// Pass single value from resp.
    it('use JSON from previous request', () => {


        let address1 = 'https://www.metaweather.com/api/location/search/?lattlong=36.96,-122.02'
        let address2 = 'https://www.metaweather.com/api/location/search/?query='
        // I. req.
        cy.request({
            method: 'GET',
            url: address1

        }).then((res)=> {
            expect(res.status).to.eq(200)
            const city = res.body[0].title
            return city
        })
        .then((city) => {
        // II. req. for the first location
            cy.request({
                method: 'GET',
                url: address2 + city
            }).then((resp) => {
                expect(resp.status).to.eq(200)
                cy.log(JSON.stringify(resp.body))
                expect(resp.body[0]).to.have.property('title', city)
            })
        })
    })
})

