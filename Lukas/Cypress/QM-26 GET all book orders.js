/// <reference types="Cypress" />

describe('get orders', () => {
    it('get all book orders', () =>{

        const address = 'https://simple-books-api.glitch.me/orders'

        cy.request({
            method: 'GET',
            url: address,
            headers: {'authorization' : "Bearer a4426cf891cb90690d9001ea2717df9b8f9b6f6f7dcf081ad516a6992730b8b9"}
        }).then((res) => {
            expect(res.status).to.eq(200)
            expect(res.body.length).to.eq(6)
            expect(res.body[0].id).to.eq('xa-5IxD6FacYH55sl7L5A')
            expect(res.body[0].customerName).to.eq('Valerin')
            expect(res.body[1].id).to.eq('Q1QtZK3_AraVcjVhtEbu2')
            expect(res.body[1].customerName).to.eq('Jackie Mayert')
            expect(res.body[2].id).to.eq('qrCYnF-VXV7wKMNfvaq5a')
            expect(res.body[2].customerName).to.eq('Dr. Janis Wiegand')
            expect(res.body[3].id).to.eq('oFJ8HApEWJvzhASc6CvaS')
            expect(res.body[3].customerName).to.eq('Julie Waters')
            expect(res.body[4].id).to.eq('S2CQymD9CK-c0pjF5DON9')
            expect(res.body[4].customerName).to.eq('Juan Ratke')
            expect(res.body[5].id).to.eq('BEmpb2Q_Ye--0sB8loUDS')
            expect(res.body[5].customerName).to.eq('Roberto Cartwright PhD')
        })
    })
})