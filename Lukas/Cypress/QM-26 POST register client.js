/// <reference types="Cypress" />

describe('register new client', () => {

    let address = 'https://simple-books-api.glitch.me/api-clients'
        let randomText = ''
        let randomEmail = ''


    it('gain api key', () => {

        // vytvorenie random email generatora
        var pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        for (var i = 0; i < 10; i++)
        randomText+=pattern.charAt(Math.floor(Math.random() * pattern.length));
        randomEmail = randomText + '@exxhample.com'

        cy.request({
            method: 'POST',
            url: address,
            body: {
                "clientName": "Valerin",
                "clientEmail": randomEmail
            }

        }).then((res)=> {

            expect(res.status).to.eq(201)
            cy.log(JSON.stringify(res))
            cy.log(JSON.stringify(res.body.accessToken))
            expect(res.allRequestResponses[0]['Response Body']).has.property('accessToken')
            expect(res.isOkStatusCode).to.eq(true)
            expect(res.statusText).to.eq('Created')
           
        })
    })
})