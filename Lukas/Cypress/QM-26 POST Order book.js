/// <reference types='Cypress' />

describe('Order book', () => {

    let accessToken = '7b4acf33f95ee40fdabc5ce7ebb8638657b9f10c5e536e24c9fbf267e17fa63d'
    let address = 'https://simple-books-api.glitch.me/orders'
    let randomText = ''
    let randomName = ''

    it('orders a book', () => {

        var pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        for (var i = 0; i < 10; i++)
        randomText+=pattern.charAt(Math.floor(Math.random() * pattern.length));
        randomName = randomText

        cy.request({
            method: 'POST',
            url: address,
            headers: { 
                'Authorization' : 'Bearer ' + accessToken
            },
            body: {
                "bookId": 1,
                "customerName": randomName
            }
        }).then((res)=> {

            cy.log(JSON.stringify(res))
            expect(res.status).to.eq(201)
            expect(res.body.created).to.eq(true)
            expect(res.body.orderId).to.not.be.null
           expect(res.statusText).to.eq('Created')
        })
    })
})