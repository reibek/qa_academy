/// <reference types="Cypress" />

describe('GET single book', () => {
    it('get single book from list of books', () => {

        const bookId = 1
        const address = 'https://simple-books-api.glitch.me/books/' + bookId
        

        cy.request({
            method: 'GET',
            url: address,

        }).then((res) => {

            expect(res.status).to.eq(200)
            expect(res.body[0]).to.not.be.null
            expect(res.body.isbn).to.eq('1780899475')
        })
        
    })
})