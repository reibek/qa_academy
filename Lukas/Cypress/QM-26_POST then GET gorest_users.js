/// <reference types='Cypress' />

describe('POST method', () => {

    it('test POST method on API', () => {

        let address = 'https://gorest.co.in/public/v1/users'
        let accessToken = '462096ce99df4771397070adfc3d779cb854bf546578c073df86c44de398dc8c'
        let randomText =  ''
        let randomEmail = ''

        var pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        for (var i = 0; i < 10; i++)
        randomText+=pattern.charAt(Math.floor(Math.random() * pattern.length));
        randomEmail = randomText

        // I. POST call
        cy.request({
            method: 'POST',
            url: address,
            body: {
                    "name": "Testkultomation",
                    "gender": "male",
                    "email": randomEmail + '@email.com',
                    "status": "active" 
            },
            headers: {
                'Authorization' : 'Bearer ' + accessToken
            }
        }).then((res) => {
            cy.log(JSON.stringify(res))
            expect(res.status).to.eq(201)
            expect(res.body.data).has.property('email', randomEmail + '@email.com')
            expect(res.body.data).has.property('name', 'Testkultomation')
            expect(res.body.data.status).to.eq('active')
            expect(res.body.data).has.property('gender', 'male')

        }).then((res) => {
            const userID = res.body.data.id
            cy.log('user id is: ' + userID)

            // II. GET call
            cy.request({
                method: 'GET',
                url: address + '/' + userID,
                headers: {
                    'Authorization' : 'Bearer ' + accessToken
                }
            }).then((res) => {
                expect(res.status).to.eq(200)
                cy.log(res.body)
                expect(res.body.data.id).to.eq(userID)
                expect(res.body.data).has.property('email', randomEmail + '@email.com')
                expect(res.body.data).has.property('name', 'Testkultomation')
                expect(res.body.data.status).to.eq('active')
                expect(res.body.data).has.property('gender', 'male')
            })
        })

    })
})