/// <reference types="Cypress" />

describe('API GET list of books', () => {
    it('get list of avaliable books', () => {

const address = 'https://simple-books-api.glitch.me/books'

        cy.request({
            method: 'GET',
            url: address

        }).then((res) =>{
            expect(res.status).to.eq(200)
            expect(res.body[0].id).to.eq(1)
            expect(res.body[0].name).to.not.be.empty
            expect(res.body[0].name).to.contain('Russian')

            expect(res.body[3].id).to.not.be.null
            expect(res.body[3].type).to.not.eq('non-fiction')
            
        })
    })
})