
describe('get all json information from array then use selected one in next request', () => {
// pass JSON ARRAY

it('get weather information for all cities', () => {

// je potrebné robiť prvé hľadanie cez lat, lon, cez query param nefunguje, vracia prázdnu array
    let address1 = 'https://www.metaweather.com/api/location/search/?lattlong=36.96,-122.02'
    let address2 = 'https://www.metaweather.com/api/location/search/?query=Am'
   
    
    cy.request({
        method: 'GET',
        url: address1

    }).then((res)=> {
        expect(res.status).to.eq(200)
        const location = res.body
        return location
    })
    .then((location) => {

        for(let i = 0; i < location.length; i++){

            cy.request({
                method: 'GET',
                url: 'https://www.metaweather.com/api/location/search/?query=' + location[i].title

            }).then((res) => {
                expect(res.status).to.eq(200)
                cy.log(JSON.stringify(res.body))
                expect(res.body[0]).to.have.property('title', location[i].title)
            })
        }
    })
})
})