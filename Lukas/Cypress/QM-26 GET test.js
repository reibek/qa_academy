/// <reference types ="Cypress" />


describe ("Get Oil news", () => {
    it ("get oil news from all sources", () => {
        cy.request({
            method : 'GET',
            url :  'https://test-oil-news.p.rapidapi.com/news',
            headers: {
                'x-rapidapi-host': 'test-oil-news.p.rapidapi.com',
                'x-rapidapi-key': '5c9ede2b70msh7f02e5cfd9a8564p14fe7djsn657c1f0c468e'
            }

        }).then((res)=>{
            expect(res.status).to.eq(200) // skontroluje, či je status 200

            cy.log(typeof(res.body[0])) // vypíše typ objetku

            // skontroluje, či sa v poli 0 nachádza niektorý z uvedených zdrojov
            expect(['guardian', 'cnbc', 'wsj', 'times']).to.include(res.body[0].source) 
            
        })
    })
})