/// <reference types="Cypress" />

let articlesStatus = new Array()
let url = "https://inardex.sk/serial/c1956"


describe("All articles from inardex", () => {
    it("looks for articles on inardex page", () => {

        for(let i = 1000; i > 999 && i < 9999; i++){
            const relativePath = url.replace(1956, i)
            cy.request({
                method: 'GET',
                url: relativePath, 
                failOnStatusCode: false
            }).then((res)=>{
                cy.log(res.status)
               if(res.status === 200){
                articlesStatus.push(res.status)
                articlesStatus.push(res.allRequestResponses[0]["Request URL"])
               }
            })
        }
        cy.log(articlesStatus) 
    })
})