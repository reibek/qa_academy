///  <reference types="Cypress" />

describe("My First Test Suite", function()
{
    it("My FirstTest case", function() {

        cy.visit("https://rahulshettyacademy.com/seleniumPractise/#/")
        cy.get(".search-keyword").type("ca")
        .then(() => {
            cy.get(".product:visible").should("have.length",4)
        })
        //Parent child chaining
        cy.get(".products").as("ProductLocator") 
        cy.get("@ProductLocator").find(".product").should("have.length",4)
        cy.get(":nth-child(3) > .product-action > button").click()
        cy.get("@ProductLocator").find(".product").eq(2).contains("ADD TO CART").click()
        

        cy.get("@ProductLocator").find(".product").each(($el, index, $list) => {

        const textVeg=$el.find("h4.product-name").text()
        if(textVeg.includes("Cashews"))
        {
        cy.wrap($el).find("button").click()  
        }
        })

    //     cy.get(".brand").then(function(logo) {

    //         cy.log(logo.text())
    //    })
    cy.get(".brand").should("have.text", "GREENKART")

    })
 
    })