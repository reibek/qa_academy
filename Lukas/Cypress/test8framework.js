/// <reference types="Cypress"/>

import Homepage from "../pageObjects/Homepage"
import Shop from "../pageObjects/Shop"

describe("Hooks", () => {
   
    before(function(){
        // spustí sa pred začatím všetkých testov v bloku
        cy.fixture('example').then(function(data){
            this.data=data
        })
    })
   
    it("handles windows", function() {

        const homePage = new Homepage()
        const shop = new Shop()

        cy.visit("https://rahulshettyacademy.com/angularpractice/")
        homePage.getEditBox().type(this.data.name)
        homePage.getGender().select(this.data.gender)
        homePage.getTwoWayDataBinding().should("have.value", this.data.name)
        homePage.getEditBox().should("have.attr", "minlength", "2" )
        homePage.getEnterpreneurButton().should("be.disabled")
        homePage.getShopTab().click()
    
        this.data.productName.forEach(function(element){
            cy.selectProduct(element)
        })

        shop.getCheckout().click()
        shop.getCheckout2().click()
        shop.getCountry().type("Slova")
        shop.selectCountry()
        shop.getCheckboxChecked().invoke("click")
        shop.submit().click()
    })
})
