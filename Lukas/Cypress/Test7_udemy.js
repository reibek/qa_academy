/// <reference types="Cypress"/>

describe("handling child windows", () => {
    it("handles windows", () => {

        cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
        cy.get("#opentab").then(function(el)
        {
            const url = el.prop('href')
            cy.visit(url)
        })
    })
})