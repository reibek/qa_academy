/// <reference types="Cypress"/>

describe("check 404 error", function() { //dostanem fail

    it("get non existing order", function() {

        const orderId = 'VVhDoD6nSQxPFYU3Po9U7Z'
        const address = 'https://simple-books-api.glitch.me/orders/' + orderId
        

        cy.request({
            method: 'GET',
            url: address,
            headers: {
                'authorization' : "Bearer a4426cf891cb90690d9001ea2717df9b8f9b6f6f7dcf081ad516a6992730b8b9" },
            failOnStatusCode: false 

        }).then((res) => {
            expect(res.status).to.eq(404)
        })
    })
})
