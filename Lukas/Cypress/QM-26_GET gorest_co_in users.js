/// <reference types="Cypress" />

describe('List of users', () => {

    it('get list of all users', () => {

        let address = 'https://gorest.co.in/public/v1/users'
        let accessToken = '462096ce99df4771397070adfc3d779cb854bf546578c073df86c44de398dc8c'

        cy.request({
            method: 'GET',
            url: address,
            headers: {
                'Authorization' : 'Bearer ' + accessToken
            }
        }).then((res) => {
            expect(res.status).to.eq(200)
            cy.log(res.body)
            expect(res.body.meta.pagination.links.previous).to.be.null
            expect(res.body.data[2].id).to.eq(5769)
            expect(res.body.data[3].gender).to.not.be.empty
            expect(res.body.data[3].gender).to.eq('female')
            

        })
    })
})