///  <reference types="Cypress" />

describe("My First Test Suite", function()
{
    it("My Second case", function() {

        cy.visit("https://rahulshettyacademy.com/seleniumPractise/#/")
        cy.get(".search-keyword").type("ca")

        //Parent child chaining
        cy.get(".products").as("ProductLocator") 

        cy.get("@ProductLocator").find(".product").each(($el, index, $list) => {

        const textVeg=$el.find("h4.product-name").text()
        if(textVeg.includes("Cashews"))
        {
        cy.wrap($el).find("button").click()  
        }
        })

        cy.get('.cart-icon > img').click()
        cy.contains("PROCEED TO CHECKOUT").click()
        cy.get(':nth-child(14)').click()
        cy.get('select').select("Slovakia")
        cy.get('.chkAgree').click()
        cy.contains('Proceed').click()
    })
})