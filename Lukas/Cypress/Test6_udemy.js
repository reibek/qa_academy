/// <reference types = "Cypress" />

describe("Test3", () => {
    it("practices cypress", () => {

        cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
        
        cy.get(".mouse-hover-content").invoke("show")
        cy.contains("Top").click({force: true})
        cy.url().should("include", "top")

    })
})