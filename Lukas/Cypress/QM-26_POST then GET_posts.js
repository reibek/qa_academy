/// <reference types="Cypress" />

describe('call POST and GET methods', () => {

    it("creates, then display user post", () => {

        let accessToken = '462096ce99df4771397070adfc3d779cb854bf546578c073df86c44de398dc8c'
        let address = 'https://gorest.co.in/public/v1/users/6455/posts'
        let randomText = ''
        let randomEmail = ''

        var pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        for (var i = 0; i < 10; i++)
        randomText+=pattern.charAt(Math.floor(Math.random() * pattern.length))
        randomEmail = randomText

        cy.request({
            method: 'POST',
            url: address,
            headers: {
                'Authorization' : 'Bearer ' + accessToken
            },
            body: {   
                    "user": "6455",
                    "title": "My new post",
                    "body": "Hello this is my new post",
                    "post": "hello post"
            }
            
        }).then((res) => {
            expect(res.status).to.eq(201)
            cy.log(JSON.stringify(res))
            expect(res.body.data).has.property('user_id', 6455)
            expect(res.body.data.id).to.not.be.null

        }).then((res) => {
            const someID = res.body.data.user_id
            const userID = res.body.data.id
            cy.log('user id is:' + someID)
            cy.log('id is: ' + userID)

            // II. get call
            
            cy.request({
                method: 'GET',
                url: address,
                headers: {
                    'Authorization' : 'Bearer ' + accessToken
                }
            }).then((res) => {
                cy.log(JSON.stringify(res))
                expect(res.status).to.eq(200)
                expect(res.body.data[0].id).to.eq(1723)
                expect(res.body.data[0].user_id).to.eq(someID)
                expect(res.body.data[0]).has.property('title', 'My new post')
                expect(res.body.data[0].body).contain('post')

            })
        })
    })
})