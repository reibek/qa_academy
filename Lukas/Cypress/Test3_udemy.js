/// <reference types="Cypress" />

describe("Test3", () => {
    it("practices cypress", () => {

        cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
        cy.get("input#checkBoxOption1").check().should("be.checked").and("have.value", "option1")
        cy.get("input#checkBoxOption1").uncheck().should("not.be.checked")
        cy.get('input[type="checkbox"]').check(["option1", "option3"]).should("be.checked").and("have.value", "option1", "option3")

        //static dropdown

        cy.get("#dropdown-class-example").select("Option2").should("have.value", "option2")

        //dynamic dropdown

        cy.get("#autocomplete").type("Slo")
        cy.get(".ui-menu-item div").each(($e1, index, $list) => {
            if($e1.text() === "Slovakia (Slovak Republic)"){
                cy.wrap($e1).click()
            }
        })
        cy.get("#autocomplete").should("have.value", "Slovakia (Slovak Republic)")

        //visible/invisible
        cy.get("#displayed-text").should("be.visible")
        cy.get("#hide-textbox").click()
        cy.get("#displayed-text").should("not.be.visible")
        cy.get("#show-textbox").click()

        // radio buttons
        cy.get('input[value="radio2"]').check().should("be.checked")


    })
})