/// <reference types = "Cypress" />

describe("Test3", () => {
    it("practices cypress", () => {

        cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
        cy.get("tr td:nth-child(2)").each(($el, index, $list) =>{
            const tabulka = $el.text()
            if(tabulka.includes("Python")){
                cy.get("tr td:nth-child(2)").eq(index).next().then((price)=>{
                    const priceText = price.text()
                    expect(priceText).to.equal("25")
                })
            }
        })


    })
})