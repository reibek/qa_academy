/// <reference types='Cypress' />

describe('get api status', () => {
    it('get api status', () => {

        const address = 'https://simple-books-api.glitch.me/status'

        cy.request ({
            method: 'GET',
            url: address
        }).then((res) => {
            expect(res.status).to.eq(200)
            cy.log(typeof(res.body))
            expect(res.body.status).to.eq('OK') // status je kľúč z json
        })
    })
})