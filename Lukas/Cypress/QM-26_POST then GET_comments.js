/// <reference types="Cypress" />

describe('call POST and GET methods', () => {

    it("creates, then display comment", () => {

        const accessToken = '462096ce99df4771397070adfc3d779cb854bf546578c073df86c44de398dc8c'
        let address = 'https://gorest.co.in/public/v1/posts?page=1/6455/comments'
        let randomText = ''
        let randomEmail = ''

        var pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        for (var i = 0; i < 10; i++)
        randomText+=pattern.charAt(Math.floor(Math.random() * pattern.length))
        randomEmail = randomText

        cy.request({
            method: 'POST',
            url: address,
            headers: {
                'Authorization' : 'Bearer ' + accessToken
            },
            body: {
                       "id": 1723,
                       "post": "hello post",
                       "user_id": 6455,
                       "title": "My new post",
                       "name": "Testkultomation",
                       "email": "uDLZsuXDwB@email.com",
                       "body": "My comment here"
            }
            
        }).then((res) => {
            expect(res.status).to.eq(201)
            cy.log(JSON.stringify(res))
        })
    })
})