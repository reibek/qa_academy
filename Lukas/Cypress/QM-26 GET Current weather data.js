/// <reference types="Cypress" />

const mesto = 'Zilina'
const appid = '5c8b767325b7f50761c0a4c4da79786d'
const address = 'https://api.openweathermap.org/data/2.5/weather?q=' + mesto + '&appid=' + appid


describe('API from open weather', () => {
    it('get current weather data', () => {


        cy.request({
            method: 'GET',
            url: address
        }).then((res) => {
            expect(res.status).to.eq(200)
            console.log(res.body.coord)
            expect(res.body.coord.lon).to.eq(19.1667)
            expect(res.body.coord.lat).to.be.greaterThan(49)
            if(res.body.coord.lon === 19.1667 && res.body.coord.lat === 49.1667) {
                console.log('správne hodnoty')
            }
            const timezone = (res.body.timezone)
            console.log(timezone)
            expect(res.body.timezone).to.eq(3600)
        })
    })
})
