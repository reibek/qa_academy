const PORT = process.env.PORT || 8000 //for deploying on heroku
const express = require('express')
const cheerio = require('cheerio')
const axios = require('axios')

const app = express()

const newspapers = [
    {
        name: 'times',
        address: 'https://www.thetimes.co.uk/business'
    },
    {
        name: 'guardian',
        address: 'https://www.theguardian.com/business/stock-markets'
    },
    {
        name: 'wsj',
        address: 'https://www.wsj.com/news/business'
    },
    {
        name: 'cnbc',
        address: 'https://www.cnbc.com/oil/'
    }
]

const articles = []

newspapers.forEach(newspaper => {
    axios.get(newspaper.address)
        .then(response => {
            const html = response.data
            const $ = cheerio.load(html)

            $('a:contains("oil")', html).each(function () {
                const title = $(this).text()
                const url = $(this).attr('href')

                articles.push({
                    title,
                    url,
                    source: newspaper.name
                })
            })
        })
})

app.get('/', (req, res) => {
    res.json('Welcome to my first API')
})

app.get('/news', (req, res) => {
    res.json(articles)
})

app.get('/news/:newspaperId', (req, res) => {
    const newspaperId = req.params.newspaperId

    const newspaperAddress = newspapers.filter(newspaper => newspaper.name == newspaperId)[0].address
    const newspaperBase = newspapers.filter(newspaper => newspaper.name == newspaperId)[0].base


    axios.get(newspaperAddress)
        .then(response => {
            const html = response.data
            const $ = cheerio.load(html)
            const specificArticles = []

            $('a:contains("oil")', html).each(function () {
                const title = $(this).text()
                const url = $(this).attr('href')
                specificArticles.push({
                    title,
                    url: newspaperBase + url,
                    source: newspaperId
                })
            })
            res.json(specificArticles)
        }).catch(err => console.log(err))
})

app.listen(PORT, () => console.log(`Starting server on PORT ${PORT}`))


