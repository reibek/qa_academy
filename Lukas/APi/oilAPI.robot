*** Settings ***
Library  RequestsLibrary
Test Setup  Log To Console    Starting API test
Test Teardown  Log to Console  ${resp.content}

*** Variables ***
${url}  https://oil-news-api.herokuapp.com

*** Test Cases ***
Skuska api
    API URL
    ${resp}=    GET On Session  oilMainPage  ${url}
    Log To Console    ${resp.status_code}

*** Keywords ***
API URL
    Create Session    oilMainPage    ${url}

