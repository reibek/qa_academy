// 104

let formular = document.querySelector("#test-form")

if(localStorage.getItem("kriminálnici") == null){
    var myArray = []
} else {
    myArray = JSON.parse(localStorage.getItem("kriminálnici"))
}



formular.addEventListener("submit", function(event) {
    event.preventDefault()

    myArray.push({
        id: "",
        firstName: event.target.elements.firstName.value,
        secondName: event.target.elements.secondName.value,
        crime: event.target.elements.crime.value
    })

    let myArrayJSON = JSON.stringify(myArray)
    localStorage.setItem("kriminálnici", myArrayJSON)
})

// 105

let tlačítko = document.querySelector(".to-list")
tlačítko.addEventListener("click", function(event){
    let myStorage = localStorage.getItem("kriminálnici")
    let myStorageJSON = JSON.parse(myStorage)

document.querySelector(".list-criminals").innerHTML = ""

    myStorageJSON.forEach(function(jedenKriminál){
        let paragraph = document.createElement("p")
        paragraph.innerHTML = `Meno: ${jedenKriminál.firstName}, <br> Priezvisko: ${jedenKriminál.secondName} <br> Zločin: ${jedenKriminál.crime}`
        document.querySelector(".list-criminals").appendChild(paragraph)
    })
})