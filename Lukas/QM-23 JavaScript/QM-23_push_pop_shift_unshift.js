// prepísanie prvku

let testArray = ["test1", "test2", "test3"]
console.log(testArray[0])
testArray[0] = "text1"
console.log(testArray)

// zapísanie prvku na posledné miesto --> push

testArray.push("Posledný prvok")
console.log(testArray)

// zapísanie prvku na prvé miesto --> unshift

testArray.unshift("Prvý prvok")
console.log(testArray)

// vymazanie prvého prvku a posledného prvku

testArray.shift()
testArray.pop()
console.log(testArray)

//splice metoda --> (1,2) odstráni druhý a tretí prvok v array - 1 hodvorí, že začína na pozícii 1 v array a 2 koľko prvkov sa má zmazať

let druhaArray = ["Jedna", "Dva", "Tri"]
druhaArray.splice(1,2)
console.log(druhaArray)

let tretiaArray = ["Jedna", "Dva", "Štyri"]
tretiaArray.splice(2,0, "Tri")
console.log(tretiaArray)

let meno1 = prompt("Zadaj meno")
let meno2 = prompt("Zadaj meno")
let meno3 = prompt("Zadaj meno")
let meno4 = prompt("Zadaj meno")
let meno5 = prompt("Zadaj meno")
let meno6 = prompt("Zadaj meno")
let pole = []

pole.push(meno1)
pole.push(meno2)
pole.push(meno3)
pole.unshift(meno4)
pole.unshift(meno5)
pole.unshift(meno6)
console.log(pole)

//alebo

let myArray = []
myArray.push(prompt("Zadaj menó"))
myArray.push(prompt("Zadaj menó"))
myArray.push(prompt("Zadaj menó"))
myArray.unshift(prompt("Zadaj menó"))
myArray.unshift(prompt("Zadaj menó"))
myArray.unshift(prompt("Zadaj menó"))
console.log(myArray)