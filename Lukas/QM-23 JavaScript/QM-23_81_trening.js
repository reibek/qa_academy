let criminals = [{
    firstName: "Martin",
    secondName: "Zelený",
    birth: 1985,
    licencePlate: "85C3322",
    address: "U sloupů 16",
    city: "České Budějovice"
    }, 
    {
    firstName: "Jana",
    secondName: "Růžová",
    birth: 1996,
    licencePlate: "93K3922",
    address: "Malská 29",
    city: "České Budějovice"
    }, 
    {
    firstName: "Anna",
    secondName: "Modrá",
    birth: 1989,
    licencePlate: "2EP6328",
    address: "Stevardská 38",
    city: "České Budějovice"
    }]

     //uložiť údaje
     let vstupneUdaje = {
        zadanyText: ""
    }

    //filtrovať údaje
   let filterr = function(podozrivy, vstupnyText){
       let hladanyPodozrivy = podozrivy.filter(function(nasPodozrivy){
           return nasPodozrivy.licencePlate.toLowerCase().includes(vstupnyText.zadanyText.toLowerCase())
       })
       

       document.querySelector("#idCriminal").innerHTML = ""

       hladanyPodozrivy.forEach(function(nasPodozrivy) {
           let paragraph = document.createElement("p")
           paragraph.innerHTML = `Meno: ${nasPodozrivy.firstName}`

           document.querySelector("#idCriminal").appendChild(paragraph)
       })
   }

    
    let SPZ = document.querySelector("#inputText")
    SPZ.addEventListener("input", function(event){
        vstupneUdaje.zadanyText = event.target.value
        filterr(criminals, vstupneUdaje)
    })

    
 