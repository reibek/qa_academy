let criminals = [{
    firstName: "Martin",
    secondName: "Zelený",
    birth: 1985,
    licencePlate: "85C3322",
    address: "U sloupů 16",
    city: "České Budějovice"
    }, 
    {
    firstName: "Jana",
    secondName: "Růžová",
    birth: 1996,
    licencePlate: "93K3922",
    address: "Malská 29",
    city: "České Budějovice"
    }, 
    {
    firstName: "Anna",
    secondName: "Modrá",
    birth: 1989,
    licencePlate: "2EP6328",
    address: "Stevardská 38",
    city: "České Budějovice"
    }]

    // uloženie dát z políčka
    let filter = {
        searchText: ""
    }

//Filter

const renderCriminals = function(oneCriminal, tryToFind){
    let arrayResult = oneCriminal.filter(function(oneSuspect){
        return oneSuspect.licencePlate.toLowerCase().includes(tryToFind.searchText.toLowerCase())
    })

 console.log(arrayResult)

 // maže predošlé vstupy
 document.querySelector("#idCriminal").innerHTML = ""


 arrayResult.forEach(function(oneSuspect) {
    let paragraph = document.createElement("p")
    paragraph.innerHTML = `Meno: ${oneSuspect.firstName}, <br> Priezvisko: ${oneSuspect.secondName}, <br> Narodený: ${oneSuspect.birth}, <br> trvalý pobyt: ${oneSuspect.address}, ${oneSuspect.city}, <br> SPZ: ${oneSuspect.licencePlate} ` 

    document.querySelector("#idCriminal").appendChild(paragraph)
 })

}





// načítanie údajov
let SPZ = document.querySelector("#inputText")

SPZ.addEventListener("input", function(event){
    filter.searchText = event.target.value
    renderCriminals(criminals, filter)
})

