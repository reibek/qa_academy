window.addEventListener("scroll", function(){
    let scrolled = window.scrollY
    let scrollable = document.documentElement.scrollHeight - this.window.innerHeight

    console.log(scrolled)
    console.log(scrollable)

    if(Math.ceil(scrolled) > 100){
        let toTop = document.querySelector(".top-page")
        toTop.style.display = "block"

        toTop.addEventListener("click", function(){
            toTop.style.display = "none"
        })
    }

    if(scrolled < 100){
        let toTop = document.querySelector(".top-page")
        toTop.style.display = "none"
    }
})

let prveTlacitko = document.querySelector(".first-item-menu")
let scrollGoal = document.querySelector(".scroll-goal").offsetTop

console.log(scrollGoal)
prveTlacitko.addEventListener("click", function(){
    window.scrollTo({
        top: scrollGoal,
        behavior: "smooth"
    })
})
