let firstBook = {
    title: "Harrky Potter a kameň mudrcov",
    author: "J.K.Rowlingová",
    published: 1997
}

let secondBook = {
    title: "Harry Potter a tajomná komnata",
    author: "J. K. Rowling",
    published: 1998
}

let resultBook = function(book){
    console.log(`Kniha od ${book.author} z roku ${book.published} sa volá ${book.title}.`)
}

resultBook(firstBook)
resultBook(secondBook)


// výzva

let person1 = {
    meno: "Dávid Shettekk-halls",
    vek: 22,
    mesto: "Terokkar Forrest",
    pohlavie: "muž"
}

let person2 = {
    meno: "Jožo Tanaris",
    vek: 16,
    mesto: "Gadzetan",
    pohlavie: "muž"
}

let person3 = {
    meno: "Hermína Jedovatá",
    vek: 17,
    mesto: "Durotar",
    pohlavie: "žena"
}

let personalInfo = function(info){
    if(info.pohlavie === "muž"){
    console.log(`Ahoj, volám sa ${info.meno}, mám ${info.vek} rokov,  a pochádzam z ${info.mesto}u.`)
    } else {
    console.log(`Ahoj, volám sa ${info.meno}, som ${info.pohlavie} a mám ${info.vek} rokov,  a pochádzam z ${info.mesto}u.`)
    }
}

personalInfo(person1)
personalInfo(person2)
personalInfo(person3)