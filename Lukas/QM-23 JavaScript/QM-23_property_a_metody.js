/*let firstName = "David"

// dĺžka stringu
console.log(firstName.length)

//veľké písmená

console.log(firstName.toUpperCase())

//odstránenie biely medzier

let secondName = " Shettek-halls "
console.log(secondName.trim())


// zahŕňa slovo?

let sentence = "David sa uč javascript."
let word = "David"
console.log(sentence.includes(word))

// odstránenie bielych medzier

let priezvisko = "   Šettek   "
console.log(priezvisko.trim())
*/

// výzva

let password = "12434admin1234"
let number = 1234

if(password.length > 13){
    console.log("Velmi silné heslo")
} else if(password.length >= 8 && password.length <= 13){
    console.log("Silné heslo")
} else {
    console.log("Slabé heslo")
}

if(password.includes(number)){
    console.log("Heslo nesmie obsahovať 1234")
} else {
    console.log("Heslo je v poriadku")
}
