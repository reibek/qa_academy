/*V domě máte aplikaci na regulaci teploty. Na dispeji se vypisuje text je "chladno" v případě,
že teplota je 10 stupňů a méně. Pokud je teplota mezi 11 a 25, tak se na displeji objeví text
"teplo". Pokud je 26 a více, tak displej bude ukazovat text "horko". */

let teplota = 16

if(teplota <= 10) {
    console.log('je chladno')
} else if(teplota >= 11 && teplota <= 25) {
    console.log('teplo')
} else if(teplota >= 26) {
    console.log('horúco')
}


/* Svědek zločinu vám popsal pachatele, který měl přes 100 kg a výšku přes 190 centimetrů.
Máte databázi zločinců a potřebujete z nich vyfiltrovat ty, kteří odpovídají těmto parametrům.
Protože svědkovi ale nemůžete zcela věřit, tak aplikace bude vypisovat i lidi, kteří mají jen
přes 100 kg nebo přes 190 centimetrů (nemusí splňovat obě dvě kritéria zároveň) */

let hmotnostPachatela = 101
let vyskaPachatela = 192

if(hmotnostPachatela >= 100 || vyskaPachatela >= 190) {
    console.log('podozrivý')
} else {
    console.log('nepreverovať')
}