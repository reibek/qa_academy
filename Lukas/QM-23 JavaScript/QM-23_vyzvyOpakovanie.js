// výzvy z kurzu 

// console.log
console.log('Ahoj ja som Harry')

// vypisovanie cez premennú 

let firstName = "Harry"
let job = "wizard"

console.log(`Ahoj, volám sa ${firstName} a pracujem ako ${job}.`)

// vypisovanie string a integer cez premennú

let fName = "Harry"
let age = 22

console.log(`Ahoj, volám sa ${fName} a mám ${age} rokov.`)
console.log(`Za 5 rokov mi bude ${age + 5}rokov.`)

// string a integer výzva

let student1 = 87
let student2 = 64
let student1Name = "Ferina"
let student2Name = "Jožina"
let maxPoints = 100

console.log(`Študent ${student1Name} dosiahol z testu ${(student1 / 100) * maxPoints} %.`)
console.log(`Študent ${student2Name} dosiahol z testu ${(student2 / 100) * maxPoints} %.`)

// boolean

let vek = 33
let child = age < 18
let adult = age >= 18
let pensioner = age >= 65
console.log("Dieťa: " + child)
console.log("Dospelý: " + adult)
console.log("Senior: " + pensioner)

// if else
let position = "jožo"
if(position === "admin" || position === "adminka"){
    console.log("Aký druh sietí poznáte?")
} else if(position === "programátor" || position === "programátorka"){
    console.log("Aký je rozdiel medzi HTML, CSS a JS?")
} else if(position === "sekretárka" || position === "sekretár"){
    console.log("Aké funkcie excelu poznáte?")
} else{
    console.log("Ste na zlom pohovore")
}

// logické operátory - termostat

let teplota = 32
if(teplota <= 10){
    console.log("Chladno")
} else if(teplota >= 11 && teplota <= 25){
    console.log("Teplo")
} else{
    console.log("Horúco")
}

let height = 189
let weight = 80
if(height >= 190 || weight >= 100){
    console.log("Podozrivý")
} else{
    console.log("Nie je podozrivý")
}

// funkcie

let adultChecker = (roky) => {
    if(roky >= 18){
        console.log("Dospelý")
    } else{
        console.log("Dieťa")
    }

    if(roky >= 18){
        console.log("Výsledok kontroly veku: " + roky + ". Môžeš vstúpiť")
    } else{
        console.log(`Výsledok kontroly veku: ${roky}. Vstup zamietnutý`)
    }
}
adultChecker(11)

// kód na dverách - funkcie

let num1 = 1
let num2 = 4
let num3 = 9
let pass = (a, b, c) => {
    if(a === num1 && b === num2 && c === num3){
        console.log("Vstup povolený")
    } else{
        console.log("Vstup zamietnutý")
    }
}
pass(1, 4, 9)

// template strings výzva

let meno = "Harry"
let priezvisko = "Potter"
let pocetRokov = 15
let prvyPriatel = "Ron"
let druhyPriatel = "Hermiona"

console.log(`Ahoj, volám sa ${meno} ${priezvisko} a mám ${pocetRokov} rokov. Mojí priatelia sú ${prvyPriatel} a ${druhyPriatel}.`)

let film = "Ospalá díra"
let director = "Tim Burton"
let cena = "Najlepší výkon vo výprave"

console.log(`Film ${film} od režíséra ${director} získal cenu ${cena}.`)

// výzvy objekty

let person1 = {
    meno: "Fero Malý",
    vek: 31,
    mesto: "Čierny Balog",
}
let person2 = {
    meno: "Jožo Dilina",
    vek:  25,
    mesto: "Jahodná"
}
let person3 = {
    meno: "Hana Pijana",
    vek: 22,
    mesto: "Drietoma"
}
console.log(`Volá sa ${person1.meno}, má ${person1.vek} rokov a pochádza z mesta menom ${person1.mesto}. `)
console.log(`Volá sa ${person2.meno}, má ${person2.vek} rokov a pochádza z mesta menom ${person2.mesto}.`)
console.log(`Volá sa ${person3.meno}, má ${person3.vek} rokov a pochádza z mesta menom ${person3.mesto}. `)

let personInfo = (person) => {
    console.log(`Volá sa ${person.meno}, má ${person.vek} rokov a pochádza z mesta menom ${person.mesto}.` )
}

personInfo(person1)

// výzva objekty - škola
let stav = false
let škola = {
    typ: "priemyselná škola",
    ulica: "Jahodová 33/12",
    mesto: "Pezinok",
    kapacita: 221,
    open: () => {
        if(stav === true){
        console.log(`Škola ${škola.typ} na ulici ${škola.ulica}, ${škola.mesto} je otvorená.`)
        } else{
            console.log(`Škola je zatvorená.`)
        }
    },
    closed: () => {
        if(stav === false){
        console.log(`Škola ${škola.typ} na ulici ${škola.ulica}, ${škola.mesto} je zatvorená.`)
        } else{
            console.log(`Škola je otvorená.`)
        }
    }
}
let výsledok = škola.closed()
console.log(výsledok)

// string length

let retazec = "fjoJOIJfewj3242Q3"
if(retazec.length > 13){
    console.log(`Veľmi silné heslo`)
} else if(retazec.length >= 8 && retazec.length <= 13){
    console.log(`Silné heslo`)
} else{
    console.log("Slabé heslo")
}
if(retazec.includes("1234")){
    console.log("Heslo nesmie obsahovať 1234!")
} else{
    console.log("Heslo je v poriadku.")
}

//kocka
let cislo1 = Math.ceil(Math.random() * 6)
let cislo2 = Math.ceil(Math.random() * 6)
let cislo3 = Math.ceil(Math.random() * 6)
let cislo4 = Math.ceil(Math.random() * 6)
let cislo5 = Math.ceil(Math.random() * 6)
let cislo6 = Math.ceil(Math.random() * 6)
let sum = cislo1 + cislo2 + cislo3 + cislo4 + cislo5 + cislo6
console.log(sum)
if(sum >= 25){
    console.log("Lucky guyy")
}  else{
    console.log("Hádž znova")
}

// password generator
let pole = ["DSafaf32", "fwdrfjIOJOI543", "POpiP54"]
let randomCislo = Math.ceil(Math.random() * 3)
let heslo = pole.length - randomCislo
console.log(pole[heslo])