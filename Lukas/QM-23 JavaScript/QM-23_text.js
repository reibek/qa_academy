let heading = document.querySelector("h1")
console.log(heading.textContent) //používanie chráni pred XSS útokom, používať radšej ako innerText
// získa len text z celého DOM
console.log(heading.innerText)
console.log(heading.innerHTML)

heading.textContent = "Nový nadpis"

let paragraphs = document.querySelectorAll("p")
console.log(paragraphs)

paragraphs.forEach(function(oneParagraph){ 
    console.log(oneParagraph.textContent)
    oneParagraph.textContent = "*******"
})

for(i = 0; i < paragraphs.length; i++){
    console.log(paragraphs[i].textContent)
    
}