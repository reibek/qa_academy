// odstránenie elementu z html

let paragraphs = document.querySelectorAll("p")
console.log(paragraphs)

paragraphs.forEach(function(oneParagraph){
    if(oneParagraph.textContent.toLowerCase().includes("nakrmiť")){
        oneParagraph.remove()
    }
})
console.log(paragraphs)

// vytvorenie nového elementu a pridanie na stránku

const newParagraph = document.createElement("p")
newParagraph.textContent = "Ahoj toto je text nového odstavca."

document.querySelector("header").appendChild(newParagraph)

const newDiv = document.createElement("div")
document.querySelector("header").appendChild(newDiv)

const secondParagraph = document.createElement("p")
secondParagraph.textContent = "Testovací text"

newDiv.appendChild(secondParagraph)

secondParagraph.append(" Nový text.")
secondParagraph.prepend("Starý text. ")

const newSpan = document.createElement("span")
newSpan.textContent = "Nový span"

secondParagraph.append(newSpan)