let pole = [{
    text: "Vyniesť smeti",
    stav: false
}, {
    text: "Nakúpiť",
    stav: false
}, {
    text: "Upratať",
    stav: true
}, {
    text: "Nakrmiť psa",
    stav: true
}, {
    text: "Nakrmiť mačku",
    stav: false
}]

let ulozisko = {
    textik: ""
}


let štenica = document.querySelector("#inputText")
štenica.addEventListener("input", function(event){
    ulozisko.textik = event.target.value
    filtrator(pole, ulozisko)
})

let filtrator = function(task, vlozenyText){
    let vysledky = task.filter(function(jedenTask){
        return jedenTask.text.toLowerCase().includes(vlozenyText.textik.toLowerCase())
    })

    document.querySelector("#idCriminal").innerHTML = ""

    vysledky.forEach(function(jedenVysledok){
        let paragraph = document.createElement("p")
        paragraph.innerHTML = `Úloha: ${jedenVysledok.text}, Stav: ${jedenVysledok.stav}`
        document.querySelector("#idCriminal").appendChild(paragraph)

    })
}