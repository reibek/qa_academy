let person1 = {
    meno: "Dávid Shettekk-halls",
    vek: 22,
    mesto: "Terokkar Forrest"
}

let person2 = {
    meno: "Jožo Tanaris",
    vek: 16,
    mesto: "Gadzetan"
}

let person3 = {
    meno: "Hermína Jedovatá",
    vek: 17,
    mesto: "Durotar"
}

console.log(`Volá sa ${person1.meno}. Je mu ${person1.vek} rokov a pochádza z mesta ${person1.mesto}.`)
console.log(`Volá sa ${person2.meno}. Je mu ${person2.vek} rokov a pochádza z mesta ${person2.mesto}.`)
console.log(`Volá sa ${person3.meno}. Je jej ${person3.vek} rokov a pochádza z mesta ${person3.mesto}.`)

