let pole = [{
    text: "Vyniesť smeti",
    stav: false
}, {
    text: "Nakúpiť",
    stav: false
}, {
    text: "Upratať",
    stav: true
}, {
    text: "Nakrmiť psa",
    stav: true
}, {
    text: "Nakrmiť mačku",
    stav: false
}]


let toDoLeft = pole.filter(function(oneToDo){
    return !oneToDo.stav
})


const odstavec = document.createElement("p")
odstavec.textContent = `Ešte ostáva splniť úloh: ${toDoLeft.length}`
document.querySelector("main").appendChild(odstavec)


for(let i = 0; i < pole.length; i++){
    const paragraf = document.createElement("p")
    paragraf.textContent = `Úloha${i + 1}: ${pole[i].text}`
    document.querySelector("main").appendChild(paragraf)
    
}

const br = document.createElement("br")
document.querySelector("main").appendChild(br)


for(let i = 0; i < pole.length; i++){
    const paragraf = document.createElement("p")
    if(pole[i].stav === false){
        paragraf.textContent = `Úloha${i + 1} nesplnena: ${pole[i].text}`
         document.querySelector("main").appendChild(paragraf)
    }
}


document.querySelector("a").addEventListener("click", function(event) {

    console.log("Klikol si úspešne!")
})

let buttons = document.querySelectorAll("button")
buttons[0].addEventListener("click", function(event){
    console.log("Kliknutie na prvé tlačítko")
})

buttons[1].addEventListener("click", function(event){
    console.log("Kliknutie na druhé tlačítko")
})

// 82.
let filters = {
    textik: ""
}


let inputId = document.querySelector("#inputId")

inputId.addEventListener("input", function(event){

    filters.textik = event.target.value
    porovnavaciFilter(pole, filters)
})

let porovnavaciFilter = function(uloha, porovnavaciText){
    let funkciaFiltra = uloha.filter(function(jednaUloha){
        return jednaUloha.text.toLowerCase().includes(porovnavaciText.textik.toLowerCase())
    })
    
//--83.
    let leftToDos = funkciaFiltra.filter(function(oneToDo){
        return oneToDo.stav === false
    })
    console.log(leftToDos.length)

    document.querySelector("#inputUlohy").innerHTML= ""
//83.--
    let paragraph = document.createElement("p")
    paragraph.textContent = `Ešte zostáva úloh: ${leftToDos.length}`
    document.querySelector("#inputUlohy").appendChild(paragraph)

    document.querySelector(".texte").innerHTML= ""

    funkciaFiltra.forEach(function(jednaUloha, i){
        let paragraph = document.createElement("p")
        paragraph.innerHTML = `Úloha ${jednaUloha.text}<br>`

        document.querySelector(".texte").appendChild(paragraph)

})
}

// vymazanie pridaných P s úlohami cez tlačítko

let mazadlo = document.querySelector("a")
    mazadlo.addEventListener("click", function(){
        let elem = document.querySelector("p")
        elem.parentNode.removeChild(elem);
})

