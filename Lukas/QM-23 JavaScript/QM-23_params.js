// funkcia s viac parametrami

let soucet = (a, b, c) => {
    return a + b + c
}

let hodnota = soucet(5, 10, 18)
console.log(hodnota)

// Defaultna (pôvodná) hodnota

let game = function (name = 'Anonym', score = 0) {
    return 'Meno: ' + name + ', Score: ' + score
}

let value = game('David', 50)
console.log(value)

// undefined - ak je niekde nevyplnená hodnota, spravidla problém v kóde
// null - zámerne ak chceme dať premennej, funkcii prázdnu hodnotu
