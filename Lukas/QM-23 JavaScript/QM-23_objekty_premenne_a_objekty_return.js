// premenné a objekty

let firstName = "David"

let myObject = {
    firstName: firstName,
    secondName: "Šhettek-halls",
    age: 32
}

console.log(myObject.firstName)

//premenné a return

let firstBook = {
    title: "Harry Potter a kameň mudrcov",
    author: "J.K.Rowling",
    published: 1997
}
let secondBook = {
    title: "Harry Potter a tajomná komnata",
    author: "J.K.Rowling",
    published: 1998
}

let bookInfo = function(book) {
    return {
        basicInfo: `${book.title} od ${book.author}`,
        publishing: `Kniha z roku ${book.published}`
    }
}

let result = bookInfo(firstBook)
console.log(result.basicInfo)
console.groupCollapsed(result.publishing)


// výzva

let school = {
    type: "gymnasium",
    street: "Polská 55",
    city: "Vyšehrad",
    capacity: 350,
    open: function(){
        console.log(`Škola ${school.type} ${school.city} je otvorená.`)
    },
    closed: function(){
        return `Škola ${school.type} ${school.city} je zatvorená.`
    }
}

console.log(school.type, school.city)

school.open()
let resultik = school.closed()
console.log(resultik)