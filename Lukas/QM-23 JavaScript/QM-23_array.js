let pole = ["test1", "test2", "test3"]

console.log(pole[1])

pole[0] = "nový prvok v poli"

console.log(pole[0])

// pridanie posledného prvku

pole.push("čtyži")

console.log(pole)

//odstránenie posledného prvku

pole.pop()
console.log(pole)

//priadenie prvého prvku

pole.unshift("nultý prvok")
console.log(pole)

//odstránenie prvého prvku

pole.shift()
console.log(pole)

let policko = ["jedna", "dva", "čtyži"]
//policko.splice(1, 2) //začína na 2 prvku a odstráni 2 prvky
console.log(policko)
policko.splice(2, 0, "Tri") //pridá tri na tretie miesto v poli
console.log(policko)