document.querySelector("#test-form").addEventListener("submit", function(event){
    //vypnutie refreshovania formulára
    event.preventDefault()
    
    //prístup k obsahu inputu
    console.log(event.target.elements.firstName.value)

    //vytvoríme odstavec s textom z políčka

    let paragraph = document.createElement("p")
    paragraph.textContent = event.target.elements.firstName.value
    document.querySelector(".from-form").appendChild(paragraph)

    //po odoslaní vymaže obsah políčka
    event.target.elements.firstName.value = ""
})


// 85. vyzva

document.querySelector("#vyzva-formular").addEventListener("submit", function(event){
    event.preventDefault()
    let meno = event.target.elements.meno.value
    let priezvisko = event.target.elements.priezvisko.value
    let email = event.target.elements.email.value

    let paragraphMeno = document.createElement("p")
    paragraphMeno.textContent = `Meno: ${meno}`
    document.querySelector(".vyzva").appendChild(paragraphMeno)

    let paragraphPriezvisko = document.createElement("p")
    paragraphPriezvisko.textContent = `Priezvisko: ${priezvisko}`
    document.querySelector(".vyzva").appendChild(paragraphPriezvisko)

    let paragraphEmail = document.createElement("p")
    paragraphEmail.textContent = `Email: ${email}`
    document.querySelector(".vyzva").appendChild(paragraphEmail)

    // skrátený zápis:
    let paragraph1 = document.createElement("p")
    paragraph1.innerHTML = `Meno: ${meno} <br> Priezvisko: ${priezvisko} <br> Email: ${email}`
    document.querySelector(".vyzva").appendChild(paragraph1)

    event.target.elements.meno.value = ""
    event.target.elements.priezvisko.value = ""
    event.target.elements.email.value = ""
})
