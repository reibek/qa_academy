// Scope - rozsah
// Global scope - premenná je použiteľná všade v kóde - pr. let prvy
// Local scope - premenná je použiteľná len v rámci bloku (napr. v rámci if podmienky) - pr. let druha

let prvy = 'prvy text'

if(true) {
    console.log(prvy) // z rodičovského scope
    let druha = 'Druhy text'
    console.log(druha)
    // nepoužieľná lebo je až v child scope  
    // console.log(tretia)

    if(true) {
        let tretia = 'tretí text'
        console.log(tretia)
        console.log(druha) // z rodičovského scope
    }
}

console.log(druha)

// Máte přístup k proměnným, které jsou definované v daném scope nebo v rodičovském scope