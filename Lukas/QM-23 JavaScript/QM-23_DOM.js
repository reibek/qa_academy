//Document object model

// querySelector (najuniverzálnejší) --  vyberie len prvý element
// querySelectorAll -- vyberie všetky elementy -- uloží ich do poľa, môžeme k nim pristupovať cez [i]
// getElementById
// get elementByClassName
/*
let heading = document.querySelector("h1") // .class  #id
console.log(heading)

let paragraphs = document.querySelectorAll("p")
console.log(paragraphs[0])

let jožo = document.getElementById("jožo")
console.log(jožo)

let myClass = document.getElementsByClassName("klasa")
console.log(myClass)
*/

//výzva 69

let id2 = document.querySelector("#test2")
console.log(id2)

let h2Nadpisy = document.querySelectorAll("h2")
console.log(h2Nadpisy[0])
console.log(h2Nadpisy[1])

let pClassTest1 = document.querySelectorAll(".test1")
console.log(pClassTest1[0])
console.log(pClassTest1[1])

let pTest1 = document.getElementsByClassName("test1")
console.log(pTest1[0])
console.log(pTest1[1])

let id = document.getElementById("test2")
console.log(id)