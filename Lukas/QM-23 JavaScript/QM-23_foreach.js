// cyklus forEach

let zamestnanci = ["David", "Harry", "Hermiona", "Ron", "Draco"]

zamestnanci.forEach(function(){
    console.log("Zamestnanec")
})

zamestnanci.forEach((person) => {
    console.log(person)
})

zamestnanci.forEach((person, index) => {
    console.log(`${index} - ${person}`)
})


let toDo = ["vyvenčiť psa", "kúiť kečup", "vymalovať izbu", " urobiť si sváču"]
toDo.forEach((úloha, index) => {
    console.log(`${index + 1}. ${úloha}`)
})