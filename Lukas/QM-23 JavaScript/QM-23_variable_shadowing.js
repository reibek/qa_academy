// variable shadowing

let myName = 'David'

if(true) {
    let myName = 'Fero'

    if(true) {
        let myName = 'Hermiona'
        console.log(myName)
    }
}

if(true) {
    console.log(myName)
}