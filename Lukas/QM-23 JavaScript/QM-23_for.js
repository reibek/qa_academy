// cyklus for

for(let i = 0; i <= 5; i++){
    console.log("testovací text")
    console.log(`${i + 1}: Testovací text`)
}

// otočený cyklus for

for(let y = 3; y >= 0; y--){
    console.log("Otočený test")
}

let zamestnanci = ["David", "Harry", "Hermiona"]

for(let i = 0; i < zamestnanci.length; i++){
    console.log(zamestnanci[i])
}

//challenge

let toDo = ["Zostríhať video", "upratať izbu", "vyluxovať"]
for(i = 0; i < toDo.length; i++){
    console.log(`${[i+1]}. ${toDo[i]}`)
}

let policko = []
for(i = 0; i < 5; i++){
    policko.push(i)
    console.log(policko)
}