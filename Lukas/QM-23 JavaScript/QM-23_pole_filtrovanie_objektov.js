
let books = [{
    title: "Harry Potter a kameň mudrcov",
    author: "J. K. Rowling",
    published: 1997
}, {
    title: "Harry Potter a tajomná komnata",
    author: "J. K. Rowling",
    published: 1998
}, {
    title: "Harry Potter a väzeň z azkabanu",
    author: "J. K. Rowling",
    published: 1999
}]

// filtrovanie na bežnom poli

let names = ["Jana", "Hana", "Dana", "Jožo"]

// obsahuje na

let arrayResults = names.filter(function(oneName){
    let find = oneName.toLowerCase().includes("na")
    return find
})
console.log(arrayResults)

//filtrovanie na poli objektov

let booksArray = books.filter(function(oneBook) {
    let find2 = oneBook.author.toLowerCase().includes("row")
    return find2
})
booksArray.forEach(function(oneResult){
    console.log(oneResult.title)
})


// challenge

let criminals = [{
    meno: "Martin",
    priezvisko: "Zelený",
    narodený: 1985,
    SPZ: "85C3322",
    adresa: "U stĺpa 16",
    mesto: "České Budejovice"
}, {
    meno: "Jana",
    priezvisko: "Zúžová",
    narodený: 1996,
    SPZ: "93K3922",
    adresa: "Malská 29",
    mesto: "České Budejovice"
}, {
    meno: "Filip",
    priezvisko: "Modrý",
    narodený: 1989,
    SPZ: "2E96328",
    adresa: "Stevardská 38",
    mesto: "České Budejovice"
}]

let vstup = prompt()
let Array1 = criminals.filter(function(jedenKriminal){
    let find = jedenKriminal.SPZ.toLocaleLowerCase().includes(vstup)
    return find
})
console.log(Array1)

Array1.forEach(function(jedenKriminal) {
    console.log(`Meno: ${jedenKriminal.meno}`)
    console.log(`Priezvisko: ${jedenKriminal.priezvisko}`)
    console.log(`Narodený: ${jedenKriminal.narodený}`)
    console.log(`SPZ: ${jedenKriminal.SPZ}`)
    console.log(`Adresa: ${jedenKriminal.adresa}`)
    console.log(`Mesto: ${jedenKriminal.mesto}`)
})