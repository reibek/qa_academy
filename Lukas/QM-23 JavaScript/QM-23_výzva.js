let premenna1 = 6
let premenna2 = 2
let premenna3 = 3

let pristup = (a, b, c) => {
    if(a === premenna1 && b === premenna2 &&  c === premenna3){
        let hlaska = 'pristup povoleny'
        return hlaska
    }
    else {
        let hlaska = 'pristup zamietnuty'
        return hlaska
    }
} 


let kodePristup = pristup(6,2,3)
console.log(kodePristup)


// scope vo funkciách
// Globálny scope - premena1, 2, 3
// Lokálny scope - a, b, c,

let premena1 = 6
let premena2 = 2
let premena3 = 3

let prist = (a, b, c) => {
    if(a === premena1 && b === premena2 &&  c === premena3){
        let hlaska = 'pristup povoleny'
        return hlaska
    }
    else {
        let hlaska = 'pristup zamietnuty'
        return hlaska
    }
} 


let kodPristup = prist(6,2,3)
console.log(kodPristup)