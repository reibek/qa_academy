let heading = document.querySelector("h1")

heading.style.color = "blue"
heading.style.fontSize = "60px"
heading.style.backgroundColor = "yellowgreen"
heading.style.fontWeight = "500"

heading.addEventListener("mouseenter", function(event){
    heading.style.color = "red"
})

heading.addEventListener("mouseleave", function(event){
    heading.style.color = "blue"
})

// 91. vývza

let stvorcek = document.createElement("div")
stvorcek.style.width = "100px"
stvorcek.style.height = "100px"
stvorcek.style.background = "white"
stvorcek.style.position = "relative"
stvorcek.style.top = "0"
stvorcek.style.left = "0"
document.querySelector("body").appendChild(stvorcek)

let newLeft = null
let newTop = null

document.querySelector("body").addEventListener("keydown", function(event) {
    if(event.key === "ArrowLeft"){
        newLeft = newLeft - 10
        stvorcek.style.left = newLeft + "px"
    } else if (event.key === "ArrowRight"){
        newLeft = newLeft + 10
        stvorcek.style.left = newLeft + "px"
    } else if (event.key === "ArrowUp"){
        newTop = newTop - 10
        stvorcek.style.top = newTop + "px"
    } else if (event.key === "ArrowDown"){
        newTop = newTop + 10
        stvorcek.style.top = newTop + "px"
    }
})

let style = getComputedStyle(stvorcek)

console.log(style.backgroundColor)
//console.log(style)


// 93.

let formular = document.createElement("form")
document.querySelector(".pole").appendChild(formular)

let textovePole = document.createElement("input")
textovePole.name = "color"
textovePole.placeholder = "Vlož názov farby"
textovePole.style.margin = "0 0 50px 0"

let bttn = document.createElement("input")
bttn.type = "submit"

document.querySelector("form").appendChild(textovePole)
document.querySelector("form").appendChild(bttn)

formular.addEventListener("submit", function(event){
    event.preventDefault()
    console.log(event.target.elements.color.value)
    let pozadie = document.querySelector("body")
    pozadie.style.backgroundColor = event.target.elements.color.value
    event.target.elements.color.value = ""
})
