// this zastupuje objekt, používame ak potrebujeme univerzálny kód. This sa používa na zastúpenie objektu -- namiesto person1, person2 použijem this


let person1 = {
    firstName: "David",
    lastName: "Shettekk-halls",
    age: 34,
    greet: function(){
        console.log(this.firstName)
        console.log(`Ahoj, ja som ${this.firstName}`)
    }
}

person1.greet()


let person2 = {
    firstName: "Harry",
    lastName: "Pokr",
    age: 15,
    greet: function(){
        console.log(this.firstName)
        console.log(`Ahoj, ja som ${this.firstName}`)
    }
}

person2.greet()

// task

let nariadenie = true

let skola = {
    typ: "obchodná akadémia",
    ulica: "Modrá 83",
    mesto: "Svit",
    kapacita: 328,
    open: function(){
        console.log(`Škola ${this.typ} ${this.mesto} je otvorená.`)
    },
    closed: function(){
        console.log(`Škola ${this.typ} ${this.mesto} je zatvorená.`)  
    }
}

if(nariadenie){
    skola.closed()
} else{
    skola.open()
}