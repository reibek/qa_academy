// indexOf -- vyhľadávanie indexu v array

let zamestnanci = ["David", "Harry", "Hermion"]

console.log(zamestnanci.indexOf("Harry"))
console.log(zamestnanci.indexOf("David"))
console.log(zamestnanci.indexOf("Hermion"))

// zadanie hodnoty, ktorá nie je v array -- konzola vypíše -1
console.log(zamestnanci.indexOf("Fero"))

if(zamestnanci.indexOf("Voldemort") === -1 ){
    console.log("Používateľ nebol nájdený")
} else {
    console.log("Používateľ nájdený")
}

/*
let meno = prompt()
if(zamestnanci.indexOf(meno) === -1){
    console.log("Používateľ nenájdený")
} else{ 
    console.log(`Používateľ ${meno} nájedný`)
    console.log(zamestnanci.indexOf(meno))
    
} */


//pokračovanie

let books = [{
    title: "Harry Potter a kameň mudrcov",
    author: "J. K. Rowling",
    published: 1997
}, {
    title: "Harry Potter a tajomná komnata",
    author: "J. K. Rowling",
    published: 1998
}, {
    title: "Harry Potter a väzeň z azkabanu",
    author: "J. K. Rowling",
    published: 1999
}]

console.log(books)
console.log(`Kniha 1, rok: ${books[0].published}`)
console.log(books[0].author)
console.log(books[0].title)


//findIndex na bežnom poli

let array1 = [8, 11, 20 ,21]

 let result = array1.findIndex(function(number){
    return number > 15
})

console.log(result)

//findIndex na poli objektov

let result2 = books.findIndex(function(onebBook){
    return onebBook.published === 1998
})
console.log(result2)

// pole výzva podozrivý v databáze

let criminals =  [{
    meno: "Martin",
    priezvisko: "Zelený",
    narodený: 1985,
    adresa: "U stĺpa 16",
    mesto: "České Budejovice"
}, {
    meno: "Jana",
    priezvisko: "Ružová",
    narodený: 1996,
    adresa: "Malská 29",
    mesto: "České Budejovice"
}, {
    meno: "Filip",
    priezvisko: "Modrý",
    narodený: 1989,
    adresa: "Stevardská 38",
    mesto: "České Budejovice"
}]

let podozrivý = prompt("Zadaj meno podozrivého: ")
console.log(podozrivý)

let vysledok = criminals.findIndex(function(jedenPodozrivý){
    return jedenPodozrivý.meno === podozrivý
})
console.log(vysledok)

let nasPodozrivy = criminals[vysledok]
console.log(nasPodozrivy)

console.log(`meno: ${nasPodozrivy.meno}`)
console.log(`priezvisko: ${nasPodozrivy.priezvisko}`)
console.log(`rok narodenia: ${nasPodozrivy.narodený}`)
console.log(`adresa: ${nasPodozrivy.adresa}`)
console.log(`mesto: ${nasPodozrivy.mesto}`)



// metoda find()
// find a bežné pole

let myArray = [1, 2, 50, 65, 11]

let vysledky = myArray.find(function(číslo){
    return číslo > 4
})
console.log(vysledky)

// find a pole objektov

let knizka = books.find(function(jednaKniha){
    if(jednaKniha.published === 1999){
        return jednaKniha.title
    }
})
console.log(knizka.title)